package Episode01;

import java.util.Scanner;

public class Episode01_general {
    static final int VALID_UNIT_SIZE = 3;
    static final int NUMBER_COUNT = 5;
    static final int MAX_OF_NUMBER = 100;
    static final int PERSON_COUNT = 5;
    static final int MAX_WEIGHT = 200;
    static final int MAX_HEIGTH = 250;

    public static void main() {
        System.out.println("Hello World!");

        int number;

        number = getPositiveIntFromConsole("Please enter a number!", 100);
        System.out.println("The number: " + number);

        //========== Control statements refactored into methods
        checkParity(number);

        divisionCheck(number, 5);
        divisionCheck(number, 3);
        divisionCheck(number, 7);

        nzpCheck(number);

        int allocationSize = getAllocationSizeForUnits();
        System.out.println("Allocated unit count: " + (allocationSize / VALID_UNIT_SIZE));

        int n = getPositiveIntFromConsole("Please enter n!", 10);
        System.out.println(n + "!=" + calculateFactorial(n));
        System.out.println("Fib(" + n + ")=" + calculateFibonacci(n));

        //========== Arrays
        int[] numberArray = createFilledArray();
        printArray("Raw array", numberArray);
        sortArray(numberArray);
        printArray("Sorted array", numberArray);

        //========== Loosely related data arrays
        int[] weightArray = new int[PERSON_COUNT];
        int[] heightArray = new int[PERSON_COUNT];
        for (int p = 0; p < PERSON_COUNT; p++) {
            System.out.println((p + 1) + ". person");
            weightArray[p] = getPositiveIntFromConsole(
                    "weight: ",
                    MAX_WEIGHT
            );
            heightArray[p] = getPositiveIntFromConsole(
                    "height: ",
                    MAX_HEIGTH
            );
        }
        printArrays("Original data (w; h)", weightArray, heightArray);
        sortArrays(weightArray, heightArray);
        printArrays("Sorted data (w; h)", weightArray, heightArray);
    }

    static void checkParity(int number) {
        if (number % 2 == 0) {
            System.out.println("even");
        } else {
            System.out.println("odd");
        }
    }

    static void divisionCheck(int number, int divisor) {
        if (number % divisor == 0) {
            System.out.println("divisible by " + divisor);
        } else {
            System.out.println("not divisible by " + divisor);
        }
    }

    static void nzpCheck(int number) {
        if (number < 0) {
            System.out.println("negative");
        } else {
            if (number > 0) {
                System.out.println("positive");
            } else {
                System.out.println("zero");
            }
        }
    }

    static int getAllocationSizeForUnits() {
        Scanner consoleScanner = new Scanner(System.in);

        System.out.println("Enter allocation for " + VALID_UNIT_SIZE + " sized units!");
        boolean isNotValid;
        int number;
        do {
            number = consoleScanner.nextInt();

            isNotValid = number <= 0 || number % VALID_UNIT_SIZE != 0;
            if (isNotValid) {
                System.out.println("Enter another number! (positive, divisible by " + VALID_UNIT_SIZE);

            }
        } while (isNotValid);

        return number;
    }

    static int getPositiveIntFromConsole(String message, int max) {
        System.out.print(message + " ");
        boolean isNotValid;
        int number;
        Scanner consoleScanner = new Scanner(System.in);
        do {
            number = consoleScanner.nextInt();

            isNotValid = number <= 0 || number > max;
            if (isNotValid) {
                System.out.print("Please try again! (0<x<=" + max + ") ");

            }
        } while (isNotValid);

        return number;
    }

    static int calculateFactorial(int n) {
        int factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial *= i;
        }

        return factorial;
    }

    static int calculateFibonacci(int n) {
        int fm2 = 0;
        int fm1 = 1;
        int fibonacci = 1;

        for (int i = 1; i <= n - 1; i++) {
            fibonacci = fm2 + fm1;
            fm2 = fm1;
            fm1 = fibonacci;
        }

        return fibonacci;
    }

    static int[] createFilledArray() {
        int[] numberArray = new int[NUMBER_COUNT];
        System.out.println("Fill up an array of " + NUMBER_COUNT + " items");
        for (int i = 0; i < NUMBER_COUNT; i++) {
            numberArray[i] = getPositiveIntFromConsole(
                    "Enter #" + i + " element:",
                    MAX_OF_NUMBER
            );
        }

        return numberArray;
    }

    static void printArray(String message, int[] array) {
        System.out.println("---------- " + message);
        for (int i = 0; i < array.length; i++) {
            System.out.println("#" + i + ": " + array[i]);
        }
    }

    static void sortArray(int[] array) {
        boolean hasChange = true;
        int temp;
        while (hasChange) {
            hasChange = false;
            for (int i = 0; i < array.length - 1; i++) {
                for (int j = i + 1; j < array.length; j++) {
                    if (array[i] > array[j]) {
                        temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;

                        hasChange = true;
                    }
                }
            }
        }
    }

    static void printArrays(String message, int[] array, int[] otherArray) {
        System.out.println("---------- " + message);
        for (int i = 0; i < array.length; i++) {
            System.out.println("#" + i + ": ("
                    + array[i]
                    + " ;"
                    + otherArray[i]
                    + ")"
            );
        }
    }

    static void sortArrays(int[] array, int[] otherArray) {
        boolean hasChange = true;
        int temp;
        while (hasChange) {
            hasChange = false;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i+1]) {
                    temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;

                    temp = otherArray[i];
                    otherArray[i] = otherArray[i+1];
                    otherArray[i+1] = temp;

                    hasChange = true;
                }
            }
        }
    }
}
