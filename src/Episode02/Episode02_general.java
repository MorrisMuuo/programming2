package Episode02;

import java.util.Scanner;

public class Episode02_general {
    static final int NUMBER_OF_PERSONS = 5;

    static public void main() {
//        Person[] persons = createAndFillPersonArray();
//        printArray(persons);
//        sortArray(persons);
//        printArray(persons);

        PersonArray personArray = new PersonArray(NUMBER_OF_PERSONS);
        //personArray.createAndFillPersonArray(NUMBER_OF_PERSONS);

        personArray.printArray();
        personArray.sortArray();
        personArray.printArray();

        personArray.get(2).setHeight(198);
    }

//    static private Person[] createAndFillPersonArray() {
//        Person[] persons = new Person[NUMBER_OF_PERSONS];
//
//        Scanner consoleScanner = new Scanner(System.in);
//
//        for (int i = 0; i < persons.length; i++) {
//            persons[i] = new Person();
//
//            System.out.println((i + 1) + "th person:");
//
//            System.out.print("weight: ");
//            persons[i].weight = consoleScanner.nextInt();
//            System.out.print("height: ");
//            persons[i].height = consoleScanner.nextInt();
//        }
//
//        return persons;
//    }
//
//    static private void printArray(Person[] persons) {
//        for (int i = 0; i < persons.length; i++) {
//            System.out.println(
//                    "#" + (i + 1) + ": ("
//                            + persons[i].weight
//                            + " ;"
//                            + persons[i].height
//                            + ")");
//        }
//    }
//
//    static private void sortArray(Person[] persons) {
//        boolean hasSwap;
//        do {
//            hasSwap = false;
//            for (int i = 0; i < persons.length - 1; i++) {
//                if (persons[i].height > persons[i + 1].height) {
//                    Person temp = persons[i];
//                    persons[i] = persons[i + 1];
//                    persons[i + 1] = temp;
//
//                    hasSwap = true;
//                }
//            }
//        } while (hasSwap);
//    }
}
