package Episode02;

import java.util.Scanner;

public class PersonArray {
    private Person[] persons;

    public PersonArray(int numberOfPersons) {
        createAndFillPersonArray(numberOfPersons);
    }

    private void createAndFillPersonArray(int numberOfPersons) {
        persons = new Person[numberOfPersons];

        Scanner consoleScanner = new Scanner(System.in);

        for (int i = 0; i < persons.length; i++) {
            persons[i] = new Person();

            System.out.println((i + 1) + "th person:");

            System.out.print("weight: ");
            persons[i].setWeight(consoleScanner.nextInt());
            System.out.print("height: ");
            persons[i].setHeight(consoleScanner.nextInt());
        }
    }

    public void printArray() {
        for (int i = 0; i < persons.length; i++) {
            System.out.println(
                    "#" + (i + 1) + ": ("
                            + persons[i].getWeight()
                            + " ;"
                            + persons[i].getHeight()
                            + ")");
        }
    }

    public void sortArray() {
        boolean hasSwap;
        do {
            hasSwap = false;
            for (int i = 0; i < persons.length - 1; i++) {
                if (persons[i].getHeight() > persons[i + 1].getHeight()) {
                    Person temp = persons[i];
                    persons[i] = persons[i + 1];
                    persons[i + 1] = temp;

                    hasSwap = true;
                }
            }
        } while (hasSwap);
    }

    public Person get(int index) {
        if (index >= 0 && index < persons.length)
            return persons[index];

        return null;
    }
}
